public class Demineur extends Plateau{
    private boolean gameOver;
    private int score;

    /**
     * Créer une nouvelle partie de démineur
     * @param nbLignes
     * @param nbColonnes
     * @param pourcentage
     */
    public Demineur(int nbLignes, int nbColonnes, int pourcentage){
        super(nbLignes, nbColonnes, pourcentage);
        this.gameOver = false;
        this.score = 0;
    }

    /**
     * Renvoie le score
     * @return (int) le score
     */
    public int getScore(){
        return this.score;
    }

    
    public void reveler(int x, int y){

    }

    public void marquer(int x, int y){

    }

    public boolean estGagnee(){
        return false;
    }

    public boolean estPerdue(){
        return false;
    }

    public void reset(){

    }

    public void affiche(){

    }

    public void nouvellePartie(){
        
    }

}

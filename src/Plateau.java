import java.util.ArrayList;

public class Plateau {
    private int nbLignes;
    private int nbColonnes;
    private int pourcentageDeBombes;
    private int nbBombes;
    private List<List<CaseIntelligente>> lePlateau;

    /**
     * Créé un nouveau plateau
     * @param nbLignes
     * @param nbColonnes
     * @param pourcentage
     */
    public Plateau(int nbLignes, int nbColonnes, int pourcentage){
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.pourcentageDeBombes = pourcentage;
        this.nbBombes = 0;
        this.lePlateau = new ArrayList<>();
    }

    /**
     * Créé toutes les cases du plateau
     */
    private void creerLesCasesVides(){
        for (int x = 0; x < nbLignes; x++){
            List<CaseIntelligente> ligne = new ArrayList<>();
            for (int y = 0; y < nbColonnes; y++){
                ligne.add(new CaseIntelligente())
            }
            this.lePlateau.add(ligne);
        }
    }


    private void rendLesCasesIntelligentes(){ /*
        remplir la liste des voisines pour chacune des CaseIntelligente 
        Faire doubel boucle for pour chaque case pour ajouter les 8 cases
        ex:
        for (int x=i-1; x<i+1; x++){
            for (int y=j-1; y<j+1; y++){
            }
        }
        */

    }

    protected void poseDesBombesAleatoirement(){

    }

    public int getNbLignes(){
        return 0;
    }

    public int getNbColonnes(){
        return 0;
    }

    public int getNbTotalBombes(){
        return 0;
    }

    public CaseIntelligente getCase(int numLigne, int numColonne){
        return null;
    }

    public void poseBombe(int x, int y){

    }

    public void reset(){
        
    }







}
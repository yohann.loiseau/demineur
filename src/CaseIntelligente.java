import java.util.ArrayList;
import java.util.List;

public class CaseIntelligente extends Case{
    private List<Case> lesVoisines;

    /**
     * Créer une nouvelle case intelligente
     */
    public CaseIntelligente(){
        super();
        this.lesVoisines = new ArrayList<>();
    }

    /**
     * Ajoute une case voisine dans la liste
     * @param uneCase
     */
    public void ajouteVoisine(Case uneCase){
        this.lesVoisines.add(uneCase);
    }

    /**
     * Renvoie le nombre de bombes parmi les cases voisines
     * @return (int) le nombre des bombes voisines
     */
    public int nombreBombesVoisines(){
        int res = 0;
        for (Case laCase : this.lesVoisines){
            if (laCase.contientUneBombe()){
                res += 1;
            }
        }
        return res;
    }

    public String toString(){
        return "";
    }
}

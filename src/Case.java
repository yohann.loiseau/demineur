public class Case {
    private boolean contientUneBombe;
    private boolean estMarquee;
    private boolean estDecouverte;

    /**
     * Creer une nouvelle case
     */
    public Case(){
        this.contientUneBombe = false;
        this.estMarquee = false;
        this.estDecouverte = false;
    }

    /**
     * Reset tous les attributs de la case
     */
    public void reset(){
        this.contientUneBombe = false;
        this.estMarquee = false;
        this.estDecouverte = false;
    }

    /**
     * Met une bombe sur la case
     */
    public void poseBombe(){
        this.contientUneBombe = true;
    }

    /**
     * Indique si la case contient une bombe
     * @return (boolean) true s'il y a une bombe, false sinon
     */
    public boolean contientUneBombe(){
        return this.contientUneBombe;
    }
    
    /**
     * Indique si la case est découverte
     * @return (boolean) true si elle est découverte, false sinon
     */
    public boolean estDecouverte(){
        return this.estDecouverte;
    }

    /**
     * Indique si la case est marquée
     * @return (boolean) true si elle est marquée, false sinon
     */
    public boolean estMarquee(){
        return this.estMarquee;
    }

    /**
     * Révele une case
     * @return (boolean) true si la case a bien pu être révélée, false sinon
     */
    public boolean reveler(){
        if (!estMarquee && !estDecouverte){
            this.estDecouverte = true;
            return true;
        }
        return false;
    }

    /**
     * Marque une case
     */
    public void marquer(){
        this.estMarquee = true;
    }
}
